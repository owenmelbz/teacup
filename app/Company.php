<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Company extends Model
{
    protected $fillable = ['name', 'email', 'logo', 'website'];

    /**
     * @return HasMany
     */
    public function employees()
    {
        return $this->hasMany(Employee::class);
    }

    /**
     * @return string
     */
    public function getDisplayNameAttribute()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLogoUrlAttribute()
    {
        if (!$this->logo) {
            return 'https://via.placeholder.com/100';
        }

        if (Str::contains($this->logo, 'http')) {
            return $this->logo;
        }

        return Storage::disk('logos')->url($this->logo);
    }

    /**
     * @return string
     */
    public function getWebsiteUrlAttribute()
    {
        if (Str::contains($this->website, 'http')) {
            return $this->website;
        }

        if (empty($this->website)) {
            return null;
        }

        return 'https://' . $this->website;
    }

    public function setLogoAttribute($value)
    {
        if ($value && method_exists($value, 'store')) {
            $path = $value->store(null, 'logos');
        } else {
            $path = $value;
        }

        $this->attributes['logo'] = $path;

        return $path;
    }
}
