<?php

namespace App\Http\Controllers;

use Exception;
use App\Company;
use App\Employee;
use Illuminate\Http\Response;
use App\Http\Requests\EmployeeRequest as Request;

class EmployeeController extends Controller
{
    const ITEMS_PER_PAGE = 10;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $items = Employee::with('company')
            ->has('company')
            ->orderBy('company_id')
            ->paginate(self::ITEMS_PER_PAGE);

        return view('employees.index')->with([
            'company_count' => Company::count(),
            'items' => $items,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('employees.create')->with([
            'companies' => Company::all()->sortBy('name'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $employee = new Employee;

        $data = $request->only($employee->fillable);

        $employee->fill($data);
        $employee->save();

        session()->flash('message', $employee->display_name . ' has been created.');

        return redirect()->route('employees.show', $employee);
    }

    /**
     * Display the specified resource.
     *
     * @param Employee $employee
     * @return Response
     */
    public function show(Employee $employee)
    {
        $employee->load('company');

        return view('employees.show')->with([
            'item' => $employee,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Employee $employee
     * @return Response
     */
    public function edit(Employee $employee)
    {
        return view('employees.edit')->with([
            'item' => $employee,
            'companies' => Company::all()->sortBy('name'),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Employee $employee
     * @return Response
     */
    public function update(Request $request, Employee $employee)
    {
        $data = $request->only($employee->fillable);

        $employee->update($data);

        session()->flash('message', $employee->display_name . ' has been updated');

        return $this->edit($employee);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Employee $employee
     * @return Response
     * @throws Exception
     */
    public function destroy(Employee $employee)
    {
        session()->flash('message', $employee->display_name . ' has been deleted - sorry no confirmations today.');

        $employee->delete();

        return redirect()->route('employees.index');
    }
}
