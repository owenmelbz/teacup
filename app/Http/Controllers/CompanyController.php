<?php

namespace App\Http\Controllers;

use Exception;
use App\Company;
use Illuminate\Http\Response;
use App\Http\Requests\CompanyRequest as Request;

class CompanyController extends Controller
{
    const ITEMS_PER_PAGE = 10;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $items = Company::withCount('employees')
            ->orderBy('updated_at', 'desc')
            ->paginate(self::ITEMS_PER_PAGE);

        return view('companies.index')->with([
            'items' => $items,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $company = new Company;

        $data = $request->only($company->fillable);

        $company->fill($data);
        $company->save();

        session()->flash('message', $company->display_name . ' has been created.');

        return redirect()->route('companies.show', $company);
    }

    /**
     * Display the specified resource.
     *
     * @param Company $company
     * @return Response
     */
    public function show(Company $company)
    {
        $company->load('employees');

        return view('companies.show')->with([
            'item' => $company,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Company $company
     * @return Response
     */
    public function edit(Company $company)
    {
        return view('companies.edit')->with([
            'item' => $company,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Company $company
     * @return Response
     */
    public function update(Request $request, Company $company)
    {
        $data = $request->only($company->fillable);

        $company->update($data);

        session()->flash('message', $company->name . ' has been updated');

        return $this->edit($company);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Company $company
     * @return Response
     * @throws Exception
     */
    public function destroy(Company $company)
    {
        session()->flash('message', $company->display_name . ' has been deleted - sorry no confirmations today.');

        $company->delete();

        return redirect()->route('companies.index');
    }
}
