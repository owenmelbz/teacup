<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Employee extends Model
{
    protected $fillable = ['first_name', 'last_name', 'email', 'phone', 'company_id'];

    /**
     * @return BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return string
     */
    public function getDisplayNameAttribute()
    {
        $parts = [$this->first_name, $this->last_name];
        $parts = array_filter($parts);

        return implode(' ', $parts);
    }
}
