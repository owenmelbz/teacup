<?php

use App\Company;
use App\Employee;
use Illuminate\Database\Seeder;

class DummyEmployeesAndCompanies extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Company::class, 20)->create()->each(function (Company $company) {
            $company->employees()->saveMany(
                factory(Employee::class, 100)->make()
            );
        });
    }
}
