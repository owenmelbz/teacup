((document) => {

    const registerUploader = container => {
        const input = container.querySelector('.custom-file-input');
        const label = container.querySelector('.custom-file-label');

        input.addEventListener('change', ({ target }) => {
            const fileName = target.files.item(0).name;

            label.innerHTML = fileName;
        })
    };

    document.querySelectorAll('.custom-file').forEach(container => registerUploader(container))

})(document);
