@extends('layouts.app')

@section('content')
<div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/companies">Companies</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add new company</li>
        </ol>
    </nav>

    <div class="row justify-content-center">
        <div class="col-md-12">

            @if ($message = session('message'))
                <div class="alert alert-success">{{ $message }}</div>
            @endif

            @if($errors->isNotEmpty())
                <div class="alert alert-danger">Sorry a validation error occurred, please fix the errors below.</div>
            @endif

            <div class="card">
                <div class="card-header">Adding a new company</div>

                <div class="card-body">

                    <div class="form-area mt-4">

                        <form name="edit-form" method="POST" action="{{ route('companies.store') }}" enctype="multipart/form-data">

                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name">Company name (required)</label>
                                <input class="form-control" id="name" name="name" value="{{ old('name') }}" >
                                @if($errors->has('name'))
                                    <div class="mt-1 alert alert-danger validation-error" role="alert">{{ $errors->first('name') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" >
                            </div>

                            <div class="form-group">
                                <label for="website">Company website</label>
                                <input type="url" class="form-control" id="website" name="website" value="{{ old('website') }}" >
                            </div>

                            <div class="form-group">
                                <label for="logo">Company logo</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input accept="image/*" type="file" class="custom-file-input" name="logo" id="logo">
                                        <label class="custom-file-label" for="logo">Choose file</label>
                                    </div>
                                </div>
                                @if($errors->has('logo'))
                                    <div class="mt-1 alert alert-danger validation-error" role="alert">{{ $errors->first('logo') }}</div>
                                @endif
                            </div>

                            <button type="submit" class="btn btn-primary">Create company</button>

                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
