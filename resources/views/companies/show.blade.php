@extends('layouts.app')

@section('content')
<div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/companies">Companies</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $item->display_name }}</li>
        </ol>
    </nav>

    <div class="row justify-content-center">

        <div class="col-md-12">

            @if ($message = session('message'))
                <div class="alert alert-success">{{ $message }}</div>
            @endif

            <div class="card">
                <div class="card-header">{{ $item->display_name }}</div>

                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <div class="position-relative">
                            <img style="width: 100px; height: auto;" class="rounded img-thumbnail" src="{{ $item->logo_url }}" alt="Logo for {{ $item->name }}">
                            <button onClick="alert('Coming soon.')" class="position-absolute btn btn-sm" style="bottom: 4px; right: -3px;">🗑</button>
                        </div>
                        <div class="p-3">
                            <strong>Email:</strong> <a href="mailto:{{ $item->email }}">{{ $item->email }}</a><br/>
                            <strong>Website:</strong> <a href="{{ $item->website_url }}" rel="noopener">{{ $item->website }}</a><br/>
                            <strong>Employee Count:</strong> {{ $item->employees->count() }}
                        </div>
                        <div>
                            <form name="delete-form" action="{{ route('companies.destroy', $item) }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="DELETE">
                                <div class="btn-group">
                                    <a href="{{ route('employees.create', ['company_id' => $item->id]) }}" class="btn btn-sm btn-outline-success">Add employee</a>
                                    <a href="{{ route('companies.edit', $item) }}" class="btn btn-sm btn-outline-info">Edit</a>
                                    <button type="submit" class="btn btn-sm btn-outline-danger">Delete</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <h2>Employees</h2>
                </div>

                @if($item->employees->isEmpty())
                    <div class="card-body">
                        <div class="alert alert-info text-center">
                            <p>You should try employing somebody, it's a right hoot.</p>
                            <p class="mb-0"><a class="btn btn-sm btn-outline-success" href="{{ route('employees.create', ['company_id' => $item->id]) }}">Add employee</a></p>
                        </div>
                    </div>
                    @else
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($item->employees as $employee)
                            <tr>
                                <td><a href="{{ route('employees.show', $employee) }}">{{ $employee->display_name }}</a></td>
                                <td><a href="mailto:{{ $employee->email }}">{{ $employee->email }}</a></td>
                                <td><a href="tel:{{ $employee->phone }}">{{ $employee->phone }}</a></td>
                                <td class="text-right">
                                    {{-- I know this is naughty - but for demo purposes. --}}
                                    <form name="delete-form" action="{{ route('employees.destroy', $item) }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <div class="btn-group">
                                            <a href="{{ route('employees.edit', $item) }}" class="btn btn-sm btn-outline-info">Edit</a>
                                            <button type="submit" class="btn btn-sm btn-outline-danger">Delete</button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                @endif

            </div>
        </div>
    </div>
</div>
@endsection
