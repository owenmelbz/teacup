@extends('layouts.app')

@section('content')
<div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/companies">Companies</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $item->display_name }}</li>
        </ol>
    </nav>

    <div class="row justify-content-center">
        <div class="col-md-12">

            @if ($message = session('message'))
                <div class="alert alert-success">{{ $message }}</div>
            @endif

            @if($errors->isNotEmpty())
                <div class="alert alert-danger">Sorry a validation error occurred, please fix the errors below.</div>
            @endif

            <div class="card">
                <div class="card-header">Editing... {{ $item->display_name }}</div>

                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <div class="position-relative">
                            <img style="width: 100px; height: auto;" class="rounded img-thumbnail" src="{{ $item->logo_url }}" alt="Logo for {{ $item->name }}">
                            <button onClick="alert('Coming soon.')" class="position-absolute btn btn-sm" style="bottom: 4px; right: -3px;">🗑</button>
                        </div>
                        <div class="p-3">
                            <strong>Email:</strong> <a href="mailto:{{ $item->email }}">{{ $item->email }}</a><br/>
                            <strong>Website:</strong> <a href="{{ $item->website_url }}" rel="noopener">{{ $item->website }}</a><br/>
                            <strong>Employee Count:</strong> {{ $item->employees->count() }}
                        </div>
                        <div>
                            <div class="btn-group">
                                <form name="delete-form" action="{{ route('companies.destroy', $item) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button type="submit" class="btn btn-sm btn-outline-danger">Delete</button>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="form-area mt-4">

                        <form name="edit-form" method="POST" action="{{ route('companies.update', $item) }}" enctype="multipart/form-data">

                            {{ csrf_field() }}

                            <input type="hidden" name="_method" value="PUT">

                            <div class="form-group">
                                <label for="name">Company name (required)</label>
                                <input class="form-control" id="name" name="name" value="{{ old('name', $item->name) }}" >
                                @if($errors->has('name'))
                                    <div class="mt-1 alert alert-danger validation-error" role="alert">{{ $errors->first('name') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" value="{{ old('email', $item->email) }}" >
                            </div>

                            <div class="form-group">
                                <label for="website">Company website</label>
                                <input type="url" class="form-control" id="website" name="website" value="{{ old('website', $item->website_url) }}" >
                            </div>

                            <div class="form-group">
                                <label for="logo">Company logo</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input accept="image/*" type="file" class="custom-file-input" name="logo" id="logo">
                                        <label class="custom-file-label" for="logo">Choose file</label>
                                    </div>
                                </div>
                                @if($errors->has('logo'))
                                    <div class="mt-1 alert alert-danger validation-error" role="alert">{{ $errors->first('logo') }}</div>
                                @endif
                            </div>

                            <button type="submit" class="btn btn-primary">Save changes</button>

                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
