@extends('layouts.app')

@section('content')
<div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/employees">Employees</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add new employee</li>
        </ol>
    </nav>

    <div class="row justify-content-center">
        <div class="col-md-12">

            @if ($message = session('message'))
                <div class="alert alert-success">{{ $message }}</div>
            @endif

            @if($errors->isNotEmpty())
                <div class="alert alert-danger">Sorry a validation error occurred, please fix the errors below.</div>
            @endif

            <div class="card">
                <div class="card-header">Adding a new employee</div>

                <div class="card-body">

                    <div class="form-area mt-4">

                        <form name="edit-form" method="POST" action="{{ route('employees.store') }}">

                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name">First name (required)</label>
                                <input class="form-control" id="first_name" name="first_name" value="{{ old('first_name') }}" >
                                @if($errors->has('first_name'))
                                <div class="mt-1 alert alert-danger validation-error" role="alert">{{ $errors->first('first_name') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="name">Last name (required)</label>
                                <input class="form-control" id="last_name" name="last_name" value="{{ old('last_name') }}" >
                                @if($errors->has('last_name'))
                                <div class="mt-1 alert alert-danger validation-error" role="alert">{{ $errors->first('last_name') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" >
                            </div>

                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="tel" class="form-control" id="phone" name="phone" value="{{ old('phone') }}" >
                            </div>

                            <div class="form-group">
                                @php $company_id = (int) old('company_id', request()->get('company_id')) @endphp
                                <label for="company_id">Company (required)</label>
                                <select class="form-control" name="company_id" id="company_id">
                                    <option value="" disabled {{ $company_id ? '' : 'selected' }}>-- Please Select</option>
                                    @foreach($companies as $company)
                                        <option {{ $company_id === $company->id ? 'selected' : '' }} value="{{ $company->id }}">{{ $company->display_name }}</option>
                                    @endforeach()
                                </select>
                                @if($errors->has('company_id'))
                                    <div class="mt-1 alert alert-danger validation-error" role="alert">{{ $errors->first('company_id') }}</div>
                                @endif
                            </div>

                            <button type="submit" class="btn btn-primary">Create employee</button>

                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
