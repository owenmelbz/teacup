@extends('layouts.app')

@section('content')
<div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/employees">Employees</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $item->display_name }}</li>
        </ol>
    </nav>

    <div class="row justify-content-center">

        <div class="col-md-12">

            @if ($message = session('message'))
                <div class="alert alert-success">{{ $message }}</div>
            @endif

            <div class="card">
                <div class="card-header">{{ $item->display_name }}</div>

                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <div class="p-3">
                            <strong>Email:</strong> <a href="mailto:{{ $item->email }}">{{ $item->email }}</a><br/>
                            <strong>Phone:</strong> <a href="tel:{{ $item->phone }}">{{ $item->phone }}</a><br/>
                            <strong>Company:</strong> <a href="{{ route('companies.show', $item->company) }}">{{ $item->company->display_name }}</a>
                        </div>
                        <div>
                            <form name="delete-form" action="{{ route('employees.destroy', $item) }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="DELETE">
                                <div class="btn-group">
                                    <a href="{{ route('employees.edit', $item) }}" class="btn btn-sm btn-outline-info">Edit</a>
                                    <button type="submit" class="btn btn-sm btn-outline-danger">Delete</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
